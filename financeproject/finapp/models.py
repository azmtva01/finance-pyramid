from django.db import models
from django.conf import settings
from datetime import datetime
from rest_framework.authtoken.models import Token


class Person(models.Model):
    name = models.CharField(verbose_name='ФИО', max_length=64, blank=True, null=True)
    user_profile = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    total_count = models.PositiveIntegerField(verbose_name='Общая сумма', default=0 )
    total_payed = models.PositiveIntegerField(verbose_name='Сколько вообщем закинул', default=0)
    total_earned = models.PositiveIntegerField(verbose_name='Сколько заработал', default=0)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, verbose_name='Человек который пригласил', blank=True, null=True)
    invited_date = models.DateTimeField(verbose_name='Дата приглашение', auto_now_add=True, blank=True, null=True)
    
    class Meta:
        verbose_name = "Person"    
        verbose_name_plural = "Persons"

    def __str__(self):
        return self.name


class Payment(models.Model):
    count = models.PositiveIntegerField(verbose_name='Сумма', default=0)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name='Человек который оплатил', blank=True, null=True )
    
    class Meta:
        verbose_name = "Payment"    
        verbose_name_plural = "Payments"


