from rest_framework import serializers
from datetime import datetime
from finapp.models import Person, Payment


class PersonSerializer(serializers.ModelSerializer):
    user_profile = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Person
        fields = ('name', 'user_profile', 'total_count', 'total_payed', 'total_earned', 'parent', 'id')

    def create(self, validated_data):
        person = Person.objects.create(**validated_data)
        person.user_profile = self.context['request_user']
        person.save()
        return person 


class PaymentSerializer(serializers.ModelSerializer):
    person = serializers.PrimaryKeyRelatedField(read_only=True)
    
    class Meta:
        model = Payment
        fields = ('count', 'person', 'id')

    def create(self, validated_data):
        payment = Payment.objects.create(**validated_data)
        person= Person.objects.get(user_profile = self.context['request_user'])
        payment.person = person 
        payment.save()
        return payment

class PersonDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ('name', 'total_count', 'total_payed', 'total_earned', 'parent', 'invited_date', 'id')