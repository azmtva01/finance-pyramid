from django.contrib import admin
from finapp.models import Person, Payment

admin.site.register(Person)
admin.site.register(Payment)