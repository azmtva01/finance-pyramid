# Generated by Django 3.0.3 on 2020-03-10 13:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('finapp', '0002_auto_20200229_1334'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='count',
            field=models.PositiveIntegerField(default=0, verbose_name='Сумма'),
        ),
        migrations.AlterField(
            model_name='person',
            name='total_count',
            field=models.PositiveIntegerField(default=0, verbose_name='Общая сумма'),
        ),
    ]
