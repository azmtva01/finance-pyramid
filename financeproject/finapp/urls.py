from django.urls import path
from finapp.views import PersonView, PaymentView

urlpatterns = [
    path('list/', PersonView.as_view({'get':'list', 'post':'create'})),
    path('list/<int:pk>/', PersonView.as_view({'get':'retrieve', 'put': 'update', 'delete':'destroy'})),
    path('payment/', PaymentView.as_view({'get':'list', 'post':'create'})),
    path('payment/<int:pk>/', PaymentView.as_view({'get':'retrieve', 'put': 'update', 'delete':'destroy'})),
    path('person/my/', PersonView.as_view({'get': 'my_person'})),
    path('payment/my/', PaymentView.as_view({'get': 'my_payment'})),
    path('payments/my/', PaymentView.as_view({'get': 'my_payments'})),

]