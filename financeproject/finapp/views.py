from django.shortcuts import render
from rest_framework.authentication import TokenAuthentication
from rest_framework.viewsets import ModelViewSet
import django_filters.rest_framework
from rest_framework import filters, status
from rest_framework.response import Response

from finapp.models import Person, Payment
from finapp.serializers import PersonSerializer, PaymentSerializer, PersonDetailSerializer
from .permission import IsPersonOwnerOrGet, IsPaymentOwnerOrGet

class PersonView(ModelViewSet):
    authentication_classes = [TokenAuthentication,]
    permission_classes = [IsPersonOwnerOrGet,]
    serializer_class = PersonSerializer
    queryset = Person.objects.all()
    lookup_field = 'pk'
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('name', 'person')
    search_fields = ('name', 'person')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    def my_person(self, request, *args, **kwargs):
        instance = Person.objects.get(user_profile=request.user)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)     
        

class PaymentView(ModelViewSet):
    serializer_class = PaymentSerializer
    authentication_classes = [TokenAuthentication,]
    permission_classes = [IsPaymentOwnerOrGet,]
    queryset = Payment.objects.all()
    lookup_field = 'pk'
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    search_fields = ('person')

    def my_payments(self, request, *args, **kwargs):
        person = Person.objects.get(user_profile=request.user)       
        instance = Payment.objects.filter(person=request.user.person)
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data) 
    
    def my_payment(self, request, *args, **kwargs):
        instance = Payment.objects.get(person=request.user.person)
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)  

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context
   
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        person = Person.objects.get(user_profile=request.user)
        if person.parent:
            person.total_count += float(request.data['count'])*0.9
            person.parent.total_count += float(request.data['count'])*0.1
            person.total_payed += float(request.data['count'])
            person.parent.total_earned += float(request.data['count'])*0.1
            person.parent.save()
            person.save()
        else:        
            person.total_count += float(request.data['count'])
            person.total_payed += float(request.data['count'])
            person.total_earned += float(request.data['count'])*0
            person.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_destroy(self, instance):
        person = instance.person    
        if person.parent:
            person.total_count -= float(instance.count) * 0.9
            person.parent.total_count -= float(instance.count) * 0.1
            person.total_payed -= float(instance.count)
            person.parent.total_earned -= float(instance.count)*0.1
            person.save()
            person.parent.save()
        else:
            person.total_count -= float(instance.count)
            person.total_payed -= float(instance.count)
            person.total_earned -= float(instance.count)*0
            person.save()
        instance.delete()

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        payment = self.get_object()
        person = Person.objects.get(pk=request.data['person'])
        if person.parent:
            person.total_count -= float(payment.count) * 0.9
            person.parent.total_count -= float(payment.count)*0.1
            person.total_count += float(request.data['count']) * 0.9
            person.parent.total_count += float(request.data['count']) * 0.1
            
            person.total_payed -= float(payment.count)
            person.parent.total_earned -= float(payment.count)*0.1
            person.total_payed += float(request.data['count'])
            person.parent.total_earned += float(request.data['count']) * 0.1
            person.save()
            person.parent.save()
        else:
            person.total_count -= float(payment.count)
            person.total_count += float(request.data['count'])
            person.total_payed -= float(payment.count)
            person.total_payed += float(request.data['count'])
            person.save()
        serializer.save()            
        return Response(serializer.data, status=status.HTTP_200_OK)



